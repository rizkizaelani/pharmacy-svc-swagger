'use strict';

var utils = require('../utils/writer.js');
var Pharmacy = require('../service/PharmacyService');

module.exports.createPharmacy = function createPharmacy (req, res, next) {
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Pharmacy.createPharmacy(body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deletePharmacy = function deletePharmacy (req, res, next) {
  var id = req.swagger.params['id'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Pharmacy.deletePharmacy(id,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllPharmacy = function getAllPharmacy (req, res, next) {
  var limit = req.swagger.params['limit'].value;
  var offset = req.swagger.params['offset'].value;
  var sort = req.swagger.params['sort'].value;
  var direction = req.swagger.params['direction'].value;
  var filter = req.swagger.params['filter'].value;
  Pharmacy.getAllPharmacy(limit,offset,sort,direction,filter)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getPharmacyById = function getPharmacyById (req, res, next) {
  var id = req.swagger.params['id'].value;
  Pharmacy.getPharmacyById(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updatePharmacy = function updatePharmacy (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Pharmacy.updatePharmacy(id,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
