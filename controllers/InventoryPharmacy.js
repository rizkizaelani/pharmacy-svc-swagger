'use strict';

var utils = require('../utils/writer.js');
var InventoryPharmacy = require('../service/InventoryPharmacyService');

module.exports.createInventory = function createInventory (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  InventoryPharmacy.createInventory(id,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteInventorPharmacy = function deleteInventorPharmacy (req, res, next) {
  var id = req.swagger.params['id'].value;
  var medicine_id = req.swagger.params['medicine_id'].value;
  var authorization = req.swagger.params['Authorization'].value;
  InventoryPharmacy.deleteInventorPharmacy(id,medicine_id,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getInventory = function getInventory (req, res, next) {
  var id = req.swagger.params['id'].value;
  var limit = req.swagger.params['limit'].value;
  var offset = req.swagger.params['offset'].value;
  var sort = req.swagger.params['sort'].value;
  var direction = req.swagger.params['direction'].value;
  var filter = req.swagger.params['filter'].value;
  InventoryPharmacy.getInventory(id,limit,offset,sort,direction,filter)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getInventoryPharmacy = function getInventoryPharmacy (req, res, next) {
  var id = req.swagger.params['id'].value;
  var medicine_id = req.swagger.params['medicine_id'].value;
  InventoryPharmacy.getInventoryPharmacy(id,medicine_id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateInventoryPharmacy = function updateInventoryPharmacy (req, res, next) {
  var id = req.swagger.params['id'].value;
  var medicine_id = req.swagger.params['medicine_id'].value;
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  InventoryPharmacy.updateInventoryPharmacy(id,medicine_id,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
