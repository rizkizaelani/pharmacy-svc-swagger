'use strict';

var utils = require('../utils/writer.js');
var Medicine = require('../service/MedicineService');

module.exports.createMedicine = function createMedicine (req, res, next) {
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Medicine.createMedicine(body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.deleteMedicine = function deleteMedicine (req, res, next) {
  var id = req.swagger.params['id'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Medicine.deleteMedicine(id,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getAllMedicine = function getAllMedicine (req, res, next) {
  var body = req.swagger.params['body'].value;
  var limit = req.swagger.params['limit'].value;
  var offset = req.swagger.params['offset'].value;
  var sort = req.swagger.params['sort'].value;
  var direction = req.swagger.params['direction'].value;
  var filter = req.swagger.params['filter'].value;
  Medicine.getAllMedicine(body,limit,offset,sort,direction,filter)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.getMedicineById = function getMedicineById (req, res, next) {
  var id = req.swagger.params['id'].value;
  Medicine.getMedicineById(id)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};

module.exports.updateMedicine = function updateMedicine (req, res, next) {
  var id = req.swagger.params['id'].value;
  var body = req.swagger.params['body'].value;
  var authorization = req.swagger.params['Authorization'].value;
  Medicine.updateMedicine(id,body,authorization)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
