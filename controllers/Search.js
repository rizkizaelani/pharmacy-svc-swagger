'use strict';

var utils = require('../utils/writer.js');
var Search = require('../service/SearchService');

module.exports.findNearestByMedicineName = function findNearestByMedicineName (req, res, next) {
  var body = req.swagger.params['body'].value;
  var limit = req.swagger.params['limit'].value;
  var offset = req.swagger.params['offset'].value;
  Search.findNearestByMedicineName(body,limit,offset)
    .then(function (response) {
      utils.writeJson(res, response);
    })
    .catch(function (response) {
      utils.writeJson(res, response);
    });
};
