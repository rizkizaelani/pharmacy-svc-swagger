'use strict';

var fs = require('fs'),
  path = require('path'),
  http = require('http');

var app = require('express');
var swaggerTools = require('swagger-tools');
var jsyaml = require('js-yaml');
var serverPort = 8080;

// swaggerRouter configuration
var options = {
  swaggerUi: path.join(__dirname, '/swagger.json'),
  controllers: path.join(__dirname, './controllers'),
  useStubs: process.env.NODE_ENV === 'development' // Conditionally turn on stubs (mock mode)
};

// The Swagger document (require it, build it programmatically, fetch it from a URL, ...)
var spec = fs.readFileSync(path.join(__dirname, 'api/swagger.yaml'), 'utf8');
var swaggerDoc = jsyaml.safeLoad(spec);

// Initialize the Swagger middleware
swaggerTools.initializeMiddleware(swaggerDoc, function (middleware) {

  // Interpret Swagger resources and attach metadata to request - must be first in swagger-tools middleware chain
  app.use(middleware.swaggerMetadata());

  // Validate Swagger requests
  app.use(middleware.swaggerValidator());

  // Route validated requests to appropriate controller
  app.use(middleware.swaggerRouter(options));

  // Serve the Swagger documents and Swagger UI
  app.use(middleware.swaggerUi());

  // Start the server
  // http.createServer(app).listen(serverPort, function () {
  //   console.log('Your server is listening on port %d (http://localhost:%d)', serverPort, serverPort);
  //   console.log('Swagger-ui is available on http://localhost:%d/docs', serverPort);
  // });

  const fastify = require('fastify')({
    logger: true
  })

  // Declare a route
  fastify.get('/', async (request, reply) => {
    return { hello: 'world' }
  })

  // Run the server!
  const start = async () => {
    try {
      await fastify.listen(8080)
      fastify.log.info(`server listening on ${fastify.server.address().port}`)
    } catch (err) {
      fastify.log.error(err)
      process.exit(1)
    }
  }
  start()

  fastify.get('/', async (request, reply) => {
    return { hello: 'world' }
  })

  // Require external modules
  const mongoose = require('mongoose')
  // Connect to DB
  mongoose.connect('mongodb://localhost/apotekdb')
    .then(() => console.log('MongoDB connected…'))
    .catch(err => console.log(err))

  const routes = require('./routes')

  routes.forEach((route, index) => {
    fastify.route(route)
  })

});
