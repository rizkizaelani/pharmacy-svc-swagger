'use strict';

// External Dependancies
const mongoose = require('mongoose');
const boom = require('boom');

/**
 * Create user
 * This can only be done by the logged in user.
 *
 * body User Created user object
 * returns User
 **/
exports.createUser = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "city": "Jakarta Pusat",
      "latitude": 6.027456183070403,
      "last_name": "Doe",
      "created_at": "2000-01-23T04:56:07.000+00:00",
      "zip_code": "10230",
      "password": "password",
      "updated_at": "2000-01-23T04:56:07.000+00:00",
      "province": "DKI Jakarta",
      "street": "Jalan. Mawar Melati",
      "subdistrict": "Tanah Abang",
      "phone_number": "08123576273",
      "id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "village": "Kebon Melati",
      "first_name": "John",
      "email": "john.doe@example.com",
      "longitude": 0.8008281904610115
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete user by id
 * This can only be done by the logged in user.
 *
 * id String The name that needs to be deleted
 * authorization String JWT Authorization (optional)
 * no response value expected for this operation
 **/
exports.deleteUser = function (id, authorization) {
  return new Promise(function (resolve, reject) {
    resolve();
  });
}


/**
 * Get All User
 * This is description
 *
 * authorization String JWT Authorization (optional)
 * limit BigDecimal  (optional)
 * offset BigDecimal  (optional)
 * sort String  (optional)
 * direction String  (optional)
 * filter List pipe separated field_name|operator|value  example:   - 'id|>=|5'  - 'name|contains|john'  valid fields:    - id   - name   - branch  valid operator:   - '='   - '<'   - '<='   - '>'    - '>='   - '!='   - 'contains' (string type only)   - 'not contains' (string type only)   - 'begins' (string type only)   - 'ends' (string type only)  (optional)
 * returns List
 **/
exports.getAllUser = function (authorization, limit, offset, sort, direction, filter) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples['application/json'] = [{
      "city": "Jakarta Pusat",
      "latitude": 6.027456183070403,
      "last_name": "Doe",
      "created_at": "2000-01-23T04:56:07.000+00:00",
      "zip_code": "10230",
      "password": "password",
      "updated_at": "2000-01-23T04:56:07.000+00:00",
      "province": "DKI Jakarta",
      "street": "Jalan. Mawar Melati",
      "subdistrict": "Tanah Abang",
      "phone_number": "08123576273",
      "id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "village": "Kebon Melati",
      "first_name": "John",
      "email": "john.doe@example.com",
      "longitude": 0.8008281904610115
    }, {
      "city": "Jakarta Pusat",
      "latitude": 6.027456183070403,
      "last_name": "Doe",
      "created_at": "2000-01-23T04:56:07.000+00:00",
      "zip_code": "10230",
      "password": "password",
      "updated_at": "2000-01-23T04:56:07.000+00:00",
      "province": "DKI Jakarta",
      "street": "Jalan. Mawar Melati",
      "subdistrict": "Tanah Abang",
      "phone_number": "08123576273",
      "id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "village": "Kebon Melati",
      "first_name": "John",
      "email": "john.doe@example.com",
      "longitude": 0.8008281904610115
    }];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get user by id
 * 
 *
 * id String The name that needs to be fetched. Use user1 for testing. 
 * authorization String JWT Authorization (optional)
 * returns User
 **/
exports.getUserByName = function (id, authorization) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "city": "Jakarta Pusat",
      "latitude": 6.027456183070403,
      "last_name": "Doe",
      "created_at": "2000-01-23T04:56:07.000+00:00",
      "zip_code": "10230",
      "password": "password",
      "updated_at": "2000-01-23T04:56:07.000+00:00",
      "province": "DKI Jakarta",
      "street": "Jalan. Mawar Melati",
      "subdistrict": "Tanah Abang",
      "phone_number": "08123576273",
      "id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "village": "Kebon Melati",
      "first_name": "John",
      "email": "john.doe@example.com",
      "longitude": 0.8008281904610115
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Logs user into the system
 * 
 *
 * body Login Created user object
 * returns UserToken
 **/
exports.loginUser = function (body) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "token": "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c"
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Updated user by id
 * This can only be done by the logged in user.
 *
 * id String name that need to be updated
 * body User Updated user object
 * authorization String JWT Authorization (optional)
 * returns User
 **/
exports.updateUser = function (id, body, authorization) {
  return new Promise(function (resolve, reject) {
    var examples = {};
    examples['application/json'] = {
      "city": "Jakarta Pusat",
      "latitude": 6.027456183070403,
      "last_name": "Doe",
      "created_at": "2000-01-23T04:56:07.000+00:00",
      "zip_code": "10230",
      "password": "password",
      "updated_at": "2000-01-23T04:56:07.000+00:00",
      "province": "DKI Jakarta",
      "street": "Jalan. Mawar Melati",
      "subdistrict": "Tanah Abang",
      "phone_number": "08123576273",
      "id": "046b6c7f-0b8a-43b9-b35d-6489e6daee91",
      "village": "Kebon Melati",
      "first_name": "John",
      "email": "john.doe@example.com",
      "longitude": 0.8008281904610115
    };
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}




module.exports = mongoose.model('User', userSchema);