'use strict';


/**
 * Create Inventory Medicine by pharmacy
 * This can only be done by the logged in user.
 *
 * id String ID pharmacy 
 * body InventoryPharmacy Created inventory pharmacy object
 * authorization String JWT Authorization (optional)
 * returns InventoryPharmacy
 **/
exports.createInventory = function(id,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "medicine_id" : "011",
  "pharmacy_id" : "0123",
  "price" : 12500.0,
  "stock" : 120
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete pharmacy
 * This can only be done by the logged in user.
 *
 * id String The name that needs to be deleted
 * medicine_id String The name that needs to be deleted
 * authorization String JWT Authorization (optional)
 * no response value expected for this operation
 **/
exports.deleteInventorPharmacy = function(id,medicine_id,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get inventory medicine by pharmacy id
 * 
 *
 * id String ID pharmacy 
 * limit BigDecimal  (optional)
 * offset BigDecimal  (optional)
 * sort String  (optional)
 * direction String  (optional)
 * filter List pipe separated field_name|operator|value example:   - 'medicine.id|=|3'   - 'medicine.name|=|Panadol'   - 'medicine.code|=|PD00132'   - 'medicine.dosis|=|John'   - 'medicine.category|=|Doe'  valid fields:    - medicine.id   - medicine.code   - medicine.name   - medicine.dosis   - medicine.category  valid operator:   - '='   - '<'   - '<='   - '>'    - '>='   - '!='   - 'contains' (string type only)   - 'not contains' (string type only)   - 'begins' (string type only)   - 'ends' (string type only)  (optional)
 * returns List
 **/
exports.getInventory = function(id,limit,offset,sort,direction,filter) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "medicine_id" : "011",
  "pharmacy_id" : "0123",
  "price" : 12500.0,
  "stock" : 120
}, {
  "medicine_id" : "011",
  "pharmacy_id" : "0123",
  "price" : 12500.0,
  "stock" : 120
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get pharmacy by id
 * 
 *
 * id String The name that needs to be fetched. Use user1 for testing. 
 * medicine_id String The name that needs to be fetched. Use user1 for testing. 
 * returns InventoryPharmacy
 **/
exports.getInventoryPharmacy = function(id,medicine_id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "medicine_id" : "011",
  "pharmacy_id" : "0123",
  "price" : 12500.0,
  "stock" : 120
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Updated pharmacy
 * This can only be done by the logged in user.
 *
 * id String name that need to be updated
 * medicine_id String name that need to be updated
 * body InventoryPharmacy Updated user object
 * authorization String JWT Authorization (optional)
 * returns InventoryPharmacy
 **/
exports.updateInventoryPharmacy = function(id,medicine_id,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "medicine_id" : "011",
  "pharmacy_id" : "0123",
  "price" : 12500.0,
  "stock" : 120
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

