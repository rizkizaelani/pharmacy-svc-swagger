'use strict';


/**
 * Create medicine
 * This can only be done by the logged in user.
 *
 * body Medicine Created medicine object
 * authorization String JWT Authorization (optional)
 * returns Medicine
 **/
exports.createMedicine = function(body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "blablabla",
  "dosis" : "blablabla",
  "name" : "Panadol",
  "description" : "blablabla",
  "id" : "59e40f6a0d1a14001dc46e47",
  "expired_at" : "blablabla",
  "category" : "blablabla"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete medicine
 * This can only be done by the logged in user.
 *
 * id String The name that needs to be deleted
 * authorization String JWT Authorization (optional)
 * no response value expected for this operation
 **/
exports.deleteMedicine = function(id,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get All medicine
 * This is description
 *
 * body Medicine Get medicine object
 * limit BigDecimal  (optional)
 * offset BigDecimal  (optional)
 * sort String  (optional)
 * direction String  (optional)
 * filter List pipe separated field_name|operator|value example:   - 'id|=|3'   - 'name|=|Panadol'   - 'code|=|PD00132'   - 'dosis|=|John'   - 'category|=|Doe'  valid fields:    - id   - code   - name   - dosis   - category  valid operator:   - '='   - '<'   - '<='   - '>'    - '>='   - '!='   - 'contains' (string type only)   - 'not contains' (string type only)   - 'begins' (string type only)   - 'ends' (string type only)  (optional)
 * returns List
 **/
exports.getAllMedicine = function(body,limit,offset,sort,direction,filter) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "code" : "blablabla",
  "dosis" : "blablabla",
  "name" : "Panadol",
  "description" : "blablabla",
  "id" : "59e40f6a0d1a14001dc46e47",
  "expired_at" : "blablabla",
  "category" : "blablabla"
}, {
  "code" : "blablabla",
  "dosis" : "blablabla",
  "name" : "Panadol",
  "description" : "blablabla",
  "id" : "59e40f6a0d1a14001dc46e47",
  "expired_at" : "blablabla",
  "category" : "blablabla"
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get medicine by id
 * 
 *
 * id String The name that needs to be fetched. Use user1 for testing. 
 * returns Medicine
 **/
exports.getMedicineById = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "blablabla",
  "dosis" : "blablabla",
  "name" : "Panadol",
  "description" : "blablabla",
  "id" : "59e40f6a0d1a14001dc46e47",
  "expired_at" : "blablabla",
  "category" : "blablabla"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Updated medicine
 * This can only be done by the logged in user.
 *
 * id String name that need to be updated
 * body Medicine Updated medicine object
 * authorization String JWT Authorization (optional)
 * returns Medicine
 **/
exports.updateMedicine = function(id,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "code" : "blablabla",
  "dosis" : "blablabla",
  "name" : "Panadol",
  "description" : "blablabla",
  "id" : "59e40f6a0d1a14001dc46e47",
  "expired_at" : "blablabla",
  "category" : "blablabla"
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

