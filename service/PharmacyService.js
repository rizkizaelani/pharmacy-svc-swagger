'use strict';


/**
 * Create Pharmacy
 * This can only be done by the logged in user.
 *
 * body Pharmacy Created pharmacy object
 * authorization String JWT Authorization (optional)
 * returns Pharmacy
 **/
exports.createPharmacy = function(body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "address" : {
    "province" : "DKI Jakarta",
    "city" : "Jakarta Pusat",
    "street" : "Jl. Asia Afrika No.8",
    "subdistrict" : "Kebon Jeruk",
    "state" : "Indonesia",
    "village" : "Kemanggisan",
    "building" : "Gelora Bung Karno",
    "zip_code" : "10270"
  },
  "phone" : "+6286-2826-382",
  "name" : "Kimia Farma",
  "id" : "59e40f6a0d1a14001dc46e47",
  "branch" : "Kemanggisan",
  "email" : "pharmacy@example.com",
  "geolocation" : {
    "latitude" : -6.1701575,
    "longitude" : 106.822161
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Delete pharmacy
 * This can only be done by the logged in user.
 *
 * id String The name that needs to be deleted
 * authorization String JWT Authorization (optional)
 * no response value expected for this operation
 **/
exports.deletePharmacy = function(id,authorization) {
  return new Promise(function(resolve, reject) {
    resolve();
  });
}


/**
 * Get All Pharmacy
 * This is description
 *
 * limit BigDecimal  (optional)
 * offset BigDecimal  (optional)
 * sort String  (optional)
 * direction String  (optional)
 * filter List pipe separated field_name|operator|value  example:   - 'id|>=|5'  - 'name|contains|john'  valid fields:    - id   - name   - branch  valid operator:   - '='   - '<'   - '<='   - '>'    - '>='   - '!='   - 'contains' (string type only)   - 'not contains' (string type only)   - 'begins' (string type only)   - 'ends' (string type only)  (optional)
 * returns List
 **/
exports.getAllPharmacy = function(limit,offset,sort,direction,filter) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ {
  "address" : {
    "province" : "DKI Jakarta",
    "city" : "Jakarta Pusat",
    "street" : "Jl. Asia Afrika No.8",
    "subdistrict" : "Kebon Jeruk",
    "state" : "Indonesia",
    "village" : "Kemanggisan",
    "building" : "Gelora Bung Karno",
    "zip_code" : "10270"
  },
  "phone" : "+6286-2826-382",
  "name" : "Kimia Farma",
  "id" : "59e40f6a0d1a14001dc46e47",
  "branch" : "Kemanggisan",
  "email" : "pharmacy@example.com",
  "geolocation" : {
    "latitude" : -6.1701575,
    "longitude" : 106.822161
  }
}, {
  "address" : {
    "province" : "DKI Jakarta",
    "city" : "Jakarta Pusat",
    "street" : "Jl. Asia Afrika No.8",
    "subdistrict" : "Kebon Jeruk",
    "state" : "Indonesia",
    "village" : "Kemanggisan",
    "building" : "Gelora Bung Karno",
    "zip_code" : "10270"
  },
  "phone" : "+6286-2826-382",
  "name" : "Kimia Farma",
  "id" : "59e40f6a0d1a14001dc46e47",
  "branch" : "Kemanggisan",
  "email" : "pharmacy@example.com",
  "geolocation" : {
    "latitude" : -6.1701575,
    "longitude" : 106.822161
  }
} ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Get pharmacy by id
 * 
 *
 * id String The name that needs to be fetched. Use user1 for testing. 
 * returns Pharmacy
 **/
exports.getPharmacyById = function(id) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "address" : {
    "province" : "DKI Jakarta",
    "city" : "Jakarta Pusat",
    "street" : "Jl. Asia Afrika No.8",
    "subdistrict" : "Kebon Jeruk",
    "state" : "Indonesia",
    "village" : "Kemanggisan",
    "building" : "Gelora Bung Karno",
    "zip_code" : "10270"
  },
  "phone" : "+6286-2826-382",
  "name" : "Kimia Farma",
  "id" : "59e40f6a0d1a14001dc46e47",
  "branch" : "Kemanggisan",
  "email" : "pharmacy@example.com",
  "geolocation" : {
    "latitude" : -6.1701575,
    "longitude" : 106.822161
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}


/**
 * Updated pharmacy
 * This can only be done by the logged in user.
 *
 * id String name that need to be updated
 * body Pharmacy Updated user object
 * authorization String JWT Authorization (optional)
 * returns Pharmacy
 **/
exports.updatePharmacy = function(id,body,authorization) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = {
  "address" : {
    "province" : "DKI Jakarta",
    "city" : "Jakarta Pusat",
    "street" : "Jl. Asia Afrika No.8",
    "subdistrict" : "Kebon Jeruk",
    "state" : "Indonesia",
    "village" : "Kemanggisan",
    "building" : "Gelora Bung Karno",
    "zip_code" : "10270"
  },
  "phone" : "+6286-2826-382",
  "name" : "Kimia Farma",
  "id" : "59e40f6a0d1a14001dc46e47",
  "branch" : "Kemanggisan",
  "email" : "pharmacy@example.com",
  "geolocation" : {
    "latitude" : -6.1701575,
    "longitude" : 106.822161
  }
};
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

