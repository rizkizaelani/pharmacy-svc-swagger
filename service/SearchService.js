'use strict';


/**
 * find nearest pharmacy by medicine name
 * 
 *
 * body SearchPharmacy Search nearest pharmacy by medicine name
 * limit BigDecimal  (optional)
 * offset BigDecimal  (optional)
 * returns List
 **/
exports.findNearestByMedicineName = function(body,limit,offset) {
  return new Promise(function(resolve, reject) {
    var examples = {};
    examples['application/json'] = [ "{}", "{}" ];
    if (Object.keys(examples).length > 0) {
      resolve(examples[Object.keys(examples)[0]]);
    } else {
      resolve();
    }
  });
}

