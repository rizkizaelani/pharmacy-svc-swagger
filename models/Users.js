// External Dependancies
const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
    id: String,
    firstname: String,
    last_name: String,
    email: String,
    password: String,
    created_at: String,
    updated_at: String,
    phone_number: String,
    latitude: Number,
    longitude: Number,
    street: String,
    building: String,
    village: String,
    subdistrict: String,
    city: String,
    province: String,
    zip_code: String,
    state: String
})

module.exports = mongoose.model('User', userSchema);